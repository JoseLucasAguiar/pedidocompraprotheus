﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebService.ViewModel
{
    public class PedidoViewModel
    {   
        public FornecedorViewModel Fornecedor { get; set; }
        public string Numero { get; set; }
        public string Solicitante{ get; set; }
        public DateTime DataEmissao { get ; set; }
        public string DataEmissaoFormatada { get { return DataEmissao.ToString("dd/MM/yyyy"); } }
        public Decimal ValorMercadoria { get; set; }
        public Decimal ValorDesconto { get; set; }
        public Decimal TotalAcrescimo{ get; set; }
        public Decimal TotalICMS { get; set; }
        public Decimal ValorIPI { get; set; }
        public Decimal ValorSeguro { get; set; }
        public Decimal ValorFrete { get; set; }
        public Decimal ValorTotal { get; set; }
        public Decimal Total { get; set; }
        public string Natureza { get; set; }

        public List<ItemPedidoViewModel> ItensPedido { get; set; }

        public PedidoViewModel()
        {
            this.ItensPedido = new List<ItemPedidoViewModel>();
        }
    }
}