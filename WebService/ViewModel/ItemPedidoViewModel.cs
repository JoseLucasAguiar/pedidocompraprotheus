﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebService.ViewModel
{
    public class ItemPedidoViewModel
    {
        public string Item { get; set; }
        public string Produto { get; set; }
        public string Descricao { get; set; }
        public string UnidadeMedida { get; set; }
        public int Quantidade{ get; set; }
        public Decimal ValorUnitario{ get; set; }
        public Decimal IPI { get; set; }
        public Decimal Total { get;  set; }
        public DateTime DataEntrega{ get; set; }
        public string Obra { get; set; }
        public string ContaOrcamentaria{ get; set; }
        public string ContaContabil{ get; set; }
        public string Conta { get; set; }
        public string DescricaoContaOrcamentaria { get; set; }
        public string TES{ get; set; }
    }
}