﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebService.ViewModel
{
    public class FornecedorViewModel
    {
        public string Nome { get; set; }
        public string Endereco { get; set; }
        public string Favorecido { get; set; }
        public string Agencia { get; set; }
        public string Banco { get; set; }
        public string DigitoAgencia { get; set; }
        public string DadosBancariosCompleto{ get { return "Ag: " + Agencia + "-" + DigitoAgencia + " Banco: " + Banco; } }
        public string Fone { get; set; }
        public string Bairro { get; set; }
        public string Municipio { get; set; }
        public string Estado { get; set; }
        public string EnderecoCompleto { get { return Endereco + ", " + Bairro + ", " + Municipio + " - " + Estado; } }
        public string CNPJ { get; set; }
        public string CEP { get; set; }
        public string IE { get; set; }

    }
}