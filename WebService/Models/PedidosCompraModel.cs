﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebService.Models
{
    public class PedidosCompraModel
    {
        public WebService.PedidoCompra.ARRAYOFPEDIDOSPENDENTES _pedidos;
        private Formatadores formatador = new Formatadores();

        public string AjusteData(string data)
        {
            return formatador.AjusteData(data);
        }

        public string AjusteDinheiroBrasil(float moeda)
        {
            return formatador.AjustaDinheiroBrasil(moeda.ToString("N2"));
        }

        public PedidosCompraModel(WebService.PedidoCompra.ARRAYOFPEDIDOSPENDENTES pendentes)
        {
            _pedidos = pendentes;
        }

    }
}