﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebService.Models
{
    public class ModuloCompraInfoModel
    {
        public short QuantidadeSCEmAberto { get { return 10; } }
        public short QuantidadePCEmAprovacao { get { return 100; } }
        public short QuantidadePCRecusadasUltimoMes { get { return 1000; } }
    }
}