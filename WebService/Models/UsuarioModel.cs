﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebService.Models
{
    public class UsuarioModel
    {
        public string Nome{ get; set; }
        public string Senha { get; set; }
        public string Filial{ get; set; }
        public string Empresa{ get; set; }
    }
}