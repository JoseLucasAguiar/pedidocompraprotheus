﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace WebService.Models
{
    public class PedidoCompraModel
    {
        public Formatadores formatador = new Formatadores();

        public WebService.PedidoCompra.PEDIDO PCWS;

        [DisplayName("Endereço Completo")]
        public string EnderecoCompleto { get { return PCWS.A2_END + PCWS.A2_BAIRRO; } }
        public string DataEmissao { get { return AjusteData(PCWS.C7_EMISSAO); } }

        public string DadosBancarios { get { return "Banco: "; } }

        public string CondicaoPagamento { get { return ""; } }

        public string Liberacao { get { return PCWS.C7_CONAPRO.Equals("L") ? "Liberado" : PCWS.C7_CONAPRO.Equals("B") ? "Pendente" : "Bloqueado"; } }
        
        public string MsgLiberado { get; set; }
        public string AjusteData(string data)
        {
            DateTime _data = new DateTime(Int32.Parse(PCWS.C7_EMISSAO.Substring(0,4)), Int32.Parse(PCWS.C7_EMISSAO.Substring(5,2)), Int32.Parse(PCWS.C7_EMISSAO.Substring(8, 2)));
            return _data.ToShortDateString();
        }

        public PedidoCompraModel(WebService.PedidoCompra.PEDIDO pc)
        {
            PCWS = pc;
        }

        /// FORMATADORES
     
        public string AjustaDinheiroBrasil(string moeda)
        {
            return formatador.AjustaDinheiroBrasil(moeda);
        }
    

    }
}