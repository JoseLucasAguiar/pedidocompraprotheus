﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebService.Models
{
    public class Formatadores
    {
        public string AjusteData(string data)
        {
            DateTime _data = new DateTime(Int32.Parse(data.Substring(0, 4)), Int32.Parse(data.Substring(5, 2)), Int32.Parse(data.Substring(8, 2)));
            return _data.ToShortDateString();
        }

        public string AjustaDinheiroBrasil(string moeda)
        {
            return "R$ " + moeda;
        }
    }
}