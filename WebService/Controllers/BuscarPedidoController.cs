﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebService.Core;
using WebService.ViewModel;
using WebService.Models;
using WebService.Enum;

namespace WebService.Controllers
{
    public class BuscarPedidoController : Controller
    {
        private static UInt32 _paginaAtual = 1;

        // GET: BuscarPedido
        public ActionResult BuscarPedido()
        {
            return View();
        }

        public ActionResult ListarPedidos(AcaoPaginacaoListagem.Paginacao? acao)
        {
            if (acao == null)
                acao = AcaoPaginacaoListagem.Paginacao.DEFAULT;

            if (acao == AcaoPaginacaoListagem.Paginacao.DEFAULT)
                _paginaAtual = 1;
            else if (acao == AcaoPaginacaoListagem.Paginacao.AVANCAR)
                _paginaAtual++;
            else if (acao == AcaoPaginacaoListagem.Paginacao.VOLTAR && _paginaAtual != 1)
                _paginaAtual--;

            PedidoWSCore pedidoWSCore = new PedidoWSCore();
            PedidosCompraModel lista = pedidoWSCore.GetPedidosPaginados(_paginaAtual);

            return View(lista);
        }
        
        public ActionResult DetalhaPedido(string numPc, AcaoDetalhamentoPedido.AcaoDetalhamentoPC acao)
        {
            PedidoCompraModel pc = null;
            
            try { 
                if (String.IsNullOrWhiteSpace(numPc)) return BuscarPedido();

                pc = new PedidoWSCore().GetPedido("01", "03", numPc, "0000"); // BUSCA A INFORMAÇÃO NO WEB SERVICE

                if (!pc.Equals(null))
                    pc.MsgLiberado = "";

                // FAZ AS VERIFICAÇÕES DE DIFERENTES TIPOS DE CHAMADAS DA TELA
                if (AcaoDetalhamentoPedido.AcaoDetalhamentoPC.APROVAR_PC_SUCESSO == acao)
                    pc.MsgLiberado = "Pedido Aprovado com Sucesso";
                else if (AcaoDetalhamentoPedido.AcaoDetalhamentoPC.APROVAR_PC_ERRO == acao)
                    pc.MsgLiberado = "Falha na Aprovação do Pedido, contate o administrador";
                else if (AcaoDetalhamentoPedido.AcaoDetalhamentoPC.REJEITAR_PC_SUCESSO == acao)
                    pc.MsgLiberado = "Pedido Rejeitado com Sucesso";
                else if (AcaoDetalhamentoPedido.AcaoDetalhamentoPC.REJEITAR_PC_ERRO == acao)
                    pc.MsgLiberado = "Falha na Rejeição do Pedido, contate o administrador";

                if (pc == null) return BuscarPedido();
            }catch(ArgumentNullException argument)
            {
                Console.WriteLine(argument.Message);
                return RedirectToAction("Index", "Home");
            }catch(Exception exception)
            {
                Console.WriteLine(exception.Message);
                return RedirectToAction("Index", "Home");
            }
                return View(pc);

        }
        
        


        [HttpPost]
        public ActionResult RejeitaPedido(string numPc)
        {
            if (String.IsNullOrWhiteSpace(numPc)) return BuscarPedido();

            PedidoWSCore pedidoWS = new PedidoWSCore();
            bool retorno = pedidoWS.RejeitaPedido(numPc);

            if (retorno)
                return DetalhaPedido(numPc, AcaoDetalhamentoPedido.AcaoDetalhamentoPC.REJEITAR_PC_SUCESSO);
            else
                return DetalhaPedido(numPc, AcaoDetalhamentoPedido.AcaoDetalhamentoPC.REJEITAR_PC_ERRO);
        }


        [HttpPost]
        public ActionResult LiberaPedido(string numPc)
        {
            if (String.IsNullOrWhiteSpace(numPc)) return BuscarPedido();
        
            PedidoWSCore pedidoWS = new PedidoWSCore();
            bool retorno = pedidoWS.LiberaPedido(numPc);

            if (retorno)
                return RedirectToAction("DetalhaPedido", new { numPc = numPc, acao = AcaoDetalhamentoPedido.AcaoDetalhamentoPC.APROVAR_PC_SUCESSO });
            else
                return RedirectToAction("DetalhaPedido", new { numPc = numPc, acao = AcaoDetalhamentoPedido.AcaoDetalhamentoPC.APROVAR_PC_ERRO });
            //return DetalhaPedido(numPc, AcaoDetalhamentoPedido.AcaoDetalhamentoPC.APROVAR_PC_ERRO);

        }
    }
}