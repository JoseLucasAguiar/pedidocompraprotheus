﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebService.Core;

namespace WebService.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            InformacoesWSCore informacoesWSCore = new InformacoesWSCore();
            
            return View(informacoesWSCore.GetInformacoes());
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ValidaLogin(string usuario, string senha, string empresa, string filial)
        {
            LoginWSCore loginWSCore = new LoginWSCore();
            bool verificador = loginWSCore.Logar(usuario, senha, empresa, filial);

            if (verificador)
            {
                Session["usuario"] = usuario;
                Session.Timeout = 10;
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View("Login");
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}