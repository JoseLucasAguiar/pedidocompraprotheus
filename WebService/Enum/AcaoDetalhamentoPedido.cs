﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebService.Enum
{
    public class AcaoDetalhamentoPedido
    {
        public enum AcaoDetalhamentoPC
        {
            ABRIR_PAGINA,
            APROVAR_PC_SUCESSO,
            APROVAR_PC_ERRO,
            REJEITAR_PC_SUCESSO,
            REJEITAR_PC_ERRO
        }
    }
}