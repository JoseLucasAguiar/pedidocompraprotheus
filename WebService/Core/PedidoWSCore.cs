﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebService.PedidoCompra;
using System.Web.Mvc;
using WebService.Models;
using System.Net.Sockets;
using WebService.Enum;

namespace WebService.Core
{
    public class PedidoWSCore
    {

        // TODO ALTERAR ESSE METODO PARA PUXAR DO WEB SERVICE
        public PedidosCompraModel GetPedidosPaginados(UInt32 pagina)
        {
            try { 
                PedidoCompra.WS_PEDIDODECOMPRASOAP pedido = new PedidoCompra.WS_PEDIDODECOMPRASOAPClient();
                PedidoCompra.LISTAPENDENCIARequest request = new PedidoCompra.LISTAPENDENCIARequest();
                PedidoCompra.ARRAYOFPEDIDOSPENDENTES pendentes;

                if (request == null) return null;
                request.EMP = "01";
                request.FILIAL = "03";
                request.USR = "000000";
                request.INDICE = "0";
                request.PAGINA = pagina;
                request.TOTALITENS = 20;

                pendentes = pedido.LISTAPENDENCIA(request).LISTAPENDENCIARESULT;
                return (new PedidosCompraModel(pendentes));
            }
            catch (System.ServiceModel.EndpointNotFoundException ex){
                Console.WriteLine(ex);
            }

            return null;
        }

        public PedidoCompraModel GetPedido(string empresa, string filial, string numero, string usuario)
        {

            try
            {
                PedidoCompra.WS_PEDIDODECOMPRASOAP pedido = new PedidoCompra.WS_PEDIDODECOMPRASOAPClient();
                PedidoCompra.GETPEDIDORequest req = new PedidoCompra.GETPEDIDORequest();
                PedidoCompra.PEDIDO pc;

                if (req == null) return null;
                req.EMP = empresa;
                req.NUMPC = numero;
                req.FILIAL = filial;
                req.USR = usuario;

                pc = pedido.GETPEDIDO(req).GETPEDIDORESULT;
                return (new PedidoCompraModel(pc));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return null;
        }
        

        public Boolean RejeitaPedido(string numPc)
        {
            if (numPc == null) throw new Exception();

            try {
                PedidoCompra.WS_PEDIDODECOMPRASOAP liberacao = new PedidoCompra.WS_PEDIDODECOMPRASOAPClient();
                PedidoCompra.LIBERAPCRequest req = new PedidoCompra.LIBERAPCRequest();

                req.COBS = ""; //OBSERVACAO
                req.COPER = "7";
                req.EMP = "01";
                req.FILIAL = "03";
                req.NUMPC = numPc;

                PedidoCompra.LIBERAPCResponse response = liberacao.LIBERAPC(req);

                if (response == null) throw new Exception();

                return response.LIBERAPCRESULT;
            }
            catch(SocketException exception){
                Console.WriteLine(exception.ErrorCode);
            }
            return false;
        }

        public Boolean LiberaPedido(string numPc)
        {
            if (numPc == null) throw new Exception();

            PedidoCompra.WS_PEDIDODECOMPRASOAP liberacao = new PedidoCompra.WS_PEDIDODECOMPRASOAPClient();
            PedidoCompra.LIBERAPCRequest req = new PedidoCompra.LIBERAPCRequest();

            req.COBS = ""; //OBSERVAÇÃO
            req.COPER = "2"; // 2 É LIBERAÇÃO E 7 REJEIÇÃO
            req.EMP = "01";
            req.FILIAL = "03";
            req.NUMPC = numPc;

            PedidoCompra.LIBERAPCResponse response = liberacao.LIBERAPC(req);

            return response.LIBERAPCRESULT;
        }
    }
}