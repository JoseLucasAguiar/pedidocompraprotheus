﻿
function rejeitaPedido(numPc, url) {

    $.ajax({
        url: "/BuscarPedido/RejeitaPedido",
        data: { 
            numPc: numPc,
        },
        type: "POST"
    }).success(function (resultado) {
        $('#msg-acoes').text("Pedido rejeitado com sucesso");

        }).fail(function (resultado) {
            $('#msg-acoes').text("Falha ao rejeitar pedido, contate o administrador");
    });
}

